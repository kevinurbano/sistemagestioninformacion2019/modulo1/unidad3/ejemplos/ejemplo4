﻿DROP DATABASE IF EXISTS ejemplo4u3;
CREATE DATABASE IF NOT EXISTS ejemplo4u3;
USE ejemplo4u3;

/* Ej1 - Crea un procedimiento simple que muestre por pantalla el texto
         "esto es un ejemplo de procedimiento" (donde la cabecera de la columna sea “mensaje”). */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej1()
    COMMENT 'procedimiento simple que muestre por pantalla el texto
            "esto es un ejemplo de procedimiento" (donde la cabecera de la columna sea “mensaje”)'
    BEGIN
      SELECT 'esto es un ejemplo de procedimiento' Mensaje;
    END //
DELIMITER ;

CALL ej1();

/* Ej2 - Crea un procedimiento donde se declaren tres variables internas, una de tipo entero con valor de 1,
         otra de tipo varchar(10) con valor nulo por defecto y otra de tipo decimal con 4 dígitos y  dos decimales con valor 10,48 por defecto.
        
        Dentro del procedimiento, modificar   el valor de la variable entera,
         la variable de tipo texto, realizar alguna operación matemática con las variables y mostrar los resultados en una select. */

DELIMITER //
  CREATE OR REPLACE PROCEDURE ej2()
    BEGIN
      DECLARE numero int DEFAULT 1;
      DECLARE texto varchar(10) DEFAULT NULL;
      DECLARE decimales decimal(4,2) DEFAULT 10.48;

     SET numero = 7;
     SET texto = 'modifica';
     SET decimales =4.20; 

     SELECT texto,numero+decimales;
    END //
DELIMITER ;

CALL ej2();

/* Ej3 - Copiar, comentar y corregir los errores en el siguiente código */

DELIMITER //
  CREATE OR REPLACE PROCEDURE ej3()
    COMMENT 'Copiar, comentar y corregir los errores en el siguiente código'
    BEGIN
      DECLARE v_caracter1 char(1);
      DECLARE forma_pago enum('metalico','tarjeta','transferencia');
      
      SET v_caracter1 = 'hola'; -- Solo se queda con el primer caracter, ademas no lo estamos mostrando que es opcional
      SET forma_pago = 1;  -- Funciona devuelve el valor por numero de orden que es 'metalico'
      -- SET forma_pago = 'cheque';  -- No funciona por que no esta establecido en la variable 
      -- SET forma_pago = 4; -- No funciona por que no existe un valor en el numero de orden 4 
      SET forma_pago = 'TARJETA'; -- Funciona aunque este en mayusculas detecta el valor como esta escrito que es en minusculas 'tarjeta'
      SELECT v_caracter1, forma_pago;
    END //
DELIMITER ;

CALL ej3();


/* Ej4 - Escribe un procedimiento que reciba un parámetro de entrada (número real) y muestre el numero en pantalla. */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej4(numero real)   
    COMMENT 'Escribe un procedimiento que reciba un parámetro de entrada (número real) y muestre el numero en pantalla. '
    BEGIN
      SELECT numero;
    END //
DELIMITER ;

call ej4(2);


/* Ej5 - Escribe un procedimiento que reciba un parámetro de entrada y asigne ese parámetro a una variable
         interna del mismo tipo. Después mostrar la variable en pantalla. */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej5(externa varchar(10))
    COMMENT 'Escribe un procedimiento que reciba un parámetro de entrada y asigne ese parámetro a una variable interna del mismo tipo. Después mostrar la variable en pantalla. '
    BEGIN
      DECLARE interna varchar(10) DEFAULT NULL;
      SET interna=externa;
      SELECT interna;
    END //
DELIMITER ;

CALL ej5('Prueba');


/* Ej6 - Escribe un procedimiento que reciba un número real de entrada y 
         muestre un mensaje indicando si el número es positivo, negativo o cero. */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej6(numero real)
    COMMENT 'Escribe un procedimiento que reciba un número real de entrada y 
             muestre un mensaje indicando si el número es positivo, negativo o cero. '
    BEGIN
      IF (numero>0) THEN
        SELECT 'El numero es positivo';
      ELSEIF (numero<0) THEN
        SELECT 'El numero es negativo';
      ELSE
        SELECT 'El numero es cero';
      END IF;
    END //
DELIMITER ;

CALL ej6(24);

/* Ej7 - Escribe una función que reciba un número entero de entrada 
         y devuelva TRUE si el número es par o FALSE en caso contrario. */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej7(numero int)
    RETURNS bool
    COMMENT 'Escribe una función que reciba un número entero de entrada 
             y devuelva TRUE si el número es par o FALSE en caso contrario.'
  BEGIN
    IF (numero%2=0) THEN
       RETURN TRUE;     
    ELSE
       RETURN  FALSE;
    END IF;

  END //
DELIMITER ;

SELECT ej7(2);

/* Ej8 - Escribe una función que devuelva el valor de la hipotenusa de un triángulo a partir de los valores de sus lados. */
DELIMITER //
  CREATE OR REPLACE FUNCTION ej8(lado1 int, lado2 int)
    RETURNS float
    COMMENT 'Escribe una función que devuelva el valor de la hipotenusa de un triángulo a partir de los valores de sus lados.'
  BEGIN    
    RETURN SQRT(lado1+lado2);
  END //
DELIMITER ;

SELECT ej8(25,10);

/* Ej9 - Modifique el procedimiento diseñado en el ejercicio anterior para que tenga un parámetro de entrada, 
         con el valor un número real, y un parámetro de salida, con una cadena de caracteres indicando si el número es positivo, negativo o cero. */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej9(numero real,OUT caracteres varchar(30))
    COMMENT 'Modifique el procedimiento diseñado en el ejercicio anterior para que tenga un parámetro de entrada, 
             con el valor un número real, y un parámetro de salida, con una cadena de caracteres indicando si el número es positivo, negativo o cero.'
    BEGIN
      SET caracteres='Cero';

      IF (numero>0) THEN
        SET caracteres='Positivo';
      ELSEIF (numero<0) THEN
        SET caracteres='Negativo';      
      END IF;
    END //
DELIMITER ;

CALL ej9(0,@resultado);

SELECT @resultado;


/* Ej10 - Escribe un procedimiento que reciba un número real de entrada, que representa el valor de la nota de un alumno,
          y muestre un mensaje indicando qué nota ha obtenido teniendo en cuenta las siguientes condiciones */

-- Procedimiento con IF
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej10_a(nota real)
    BEGIN
      DECLARE resultado varchar(20) DEFAULT 'Insuficiente';

      IF (nota>=5 AND nota<6) THEN
        SET resultado = 'Aprobado';
      ELSEIF (nota>=6 AND nota<7) THEN
        SET resultado = 'Bien';
      ELSEIF (nota>=7 AND nota<9) THEN
        SET resultado = 'Notable';
      ELSEIF (nota>=9 AND nota<=10) THEN
        SET resultado = 'sobresaliente';
      ELSEIF (nota>10 OR nota<0) THEN
        SET resultado = 'No valido';
      END IF;

      SELECT resultado;
    END //
DELIMITER ;

CALL ej10_a(0);

-- Procedimiento con CASE
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej10_b(nota int)
    BEGIN
      DECLARE resultado varchar(20) DEFAULT NULL;

      CASE   
        WHEN nota>0 AND nota<5 THEN
          SET resultado= 'Insuficiente';           
        WHEN nota>=5 AND nota<6 THEN 
          SET resultado= 'Aprobado';
        WHEN nota>=6 AND nota<7 THEN
          SET resultado= 'Bien';   
        WHEN nota>=7 AND nota<9 THEN
          SET resultado= 'Notable';
        WHEN nota>=9 AND nota<=10 THEN
          SET resultado= 'Sobresaliente';        
        ELSE 
          SET resultado='No valido';
      END CASE;
      
      SELECT resultado;
    END //
DELIMITER ;

CALL ej10_b(5);

/* 
  Ej11 - Escribe un procedimiento que me cree una tabla llamada notas en caso de que no exista. 
         El procedimiento recibirá dos argumentos de entrada que son nombre y nota (numero real). 
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej11(nombre varchar(20),numero real)
    COMMENT 'Escribe un procedimiento que me cree una tabla llamada notas en caso de que no exista. 
             El procedimiento recibirá dos argumentos de entrada que son nombre y nota (numero real).'
    BEGIN        
        
      CREATE TABLE IF NOT EXISTS notas(      
        id int AUTO_INCREMENT,
        Nombre varchar(30),
        Nota  real,
        PRIMARY KEY(id)
        );

      INSERT INTO notas (Nombre, Nota)
        VALUES (nombre, numero);       

    END //
DELIMITER ;

CALL ej11('kevin',24);

SELECT * FROM notas n;

/*
  Ej12 - Escribe una función que reciba como parámetro de entrada un valor numérico que represente un día de la semana 
         y que devuelva una cadena de caracteres con el nombre del día de la semana correspondiente. Por ejemplo,
         para el valor de entrada 1 debería devolver la cadena lunes.  
*/
DELIMITER //
  CREATE OR REPLACE FUNCTION ej12(num int)    
    RETURNS varchar(20)
    COMMENT 'Escribe una función que reciba como parámetro de entrada un valor numérico que represente un día de la semana 
             y que devuelva una cadena de caracteres con el nombre del día de la semana correspondiente. Por ejemplo,
             para el valor de entrada 1 debería devolver la cadena lunes.'
  BEGIN
    DECLARE dia varchar(20) DEFAULT 'No valido';
    CASE num
      WHEN 1 THEN 
       SET dia='Lunes';
      WHEN 2 THEN
       SET dia='Martes';
      WHEN 3 THEN
       SET dia='Miercoles';
      WHEN 4 THEN
       SET dia='Jueves';
      WHEN 5 THEN
       SET dia='Viernes';
      WHEN 6 THEN
       SET dia='Sabado';
      WHEN 7 THEN
       SET dia='Domingo'; 
      ELSE
       SET dia='No valido';
    END CASE;
    
    RETURN dia;
  END //
DELIMITER ;

SELECT ej12(8);

/* 
  Ej13 - Crear un procedimiento almacenado que le pasemos un nombre de alumno
         y me devuelva cuantos alumnos hay en la tabla notas con ese nombre.
         Realizarlo sin utilizar cursores. 
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej13(alumno varchar(20))
    COMMENT 'Crear un procedimiento almacenado que le pasemos un nombre de alumno 
             y me devuelva cuantos alumnos hay en la tabla notas con ese nombre.
             Realizarlo sin utilizar cursores.'
    BEGIN
      SELECT COUNT(*) num_Alumnos FROM notas n WHERE n.Nombre=alumno;
    END //
DELIMITER ;

CALL ej13('kevin');

/*
  Ej14 - Crear un procedimiento almacenado que le pasemos un nombre de alumno y me devuelva 
         cuantos alumnos hay en la tabla notas con ese nombre.
         Realizarlo utilizando cursores (sin manejar excepciones).   
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej14(alumno varchar(30))
    COMMENT 'Crear un procedimiento almacenado que le pasemos un nombre de alumno y me devuelva 
             cuantos alumnos hay en la tabla notas con ese nombre.
             Realizarlo utilizando cursores (sin manejar excepciones)'
    BEGIN

      DECLARE numAlumnos int DEFAULT 0;
      DECLARE result varchar(30) DEFAULT NULL;
      DECLARE cont int DEFAULT 0;
      
      -- Cursor
      DECLARE cursor1 CURSOR FOR
        SELECT n.Nombre FROM notas n WHERE n.Nombre=alumno;   

      SELECT COUNT(*) INTO numAlumnos FROM notas n WHERE n.Nombre=alumno;     
      

      OPEN cursor1;
    
      WHILE (numAlumnos>0) DO  
        FETCH cursor1 INTO result;            
        SET cont = cont+1;
        SET numAlumnos = numAlumnos-1;       
      END WHILE;
      
      CLOSE cursor1;  
      
      SELECT cont;
          
    END //
DELIMITER ;

CALL ej14('kevin'); 

/*
  Ej15 - Crear un procedimiento almacenado que le pasemos 
         un nombre de alumno y me devuelva cuantos alumnos
         hay en la tabla notas con ese nombre.
         Realizarlo utilizando cursores (manejando excepciones).   
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej15(alumno varchar(30))
    COMMENT 'Crear un procedimiento almacenado que le pasemos 
             un nombre de alumno y me devuelva cuantos alumnos
             hay en la tabla notas con ese nombre. 
             Realizarlo utilizando cursores (manejando excepciones). '
    BEGIN
      DECLARE fin int DEFAULT 0;
      DECLARE cont int DEFAULT 0;
      DECLARE result varchar(30);

      DECLARE cursor1 CURSOR FOR SELECT n.Nombre FROM notas n WHERE n.Nombre=alumno;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin=1;
     
      OPEN cursor1;
      FETCH cursor1 INTO result;

      WHILE fin=0 DO 
        SET cont = cont+1;
        FETCH cursor1 INTO result;
      END WHILE;
            
      SELECT cont;
    END //
DELIMITER ;

CALL ej15('kevin');

/*
  Ej16 -Crear un procedimiento almacenado que me cree una tabla
        denominada usuarios solamente si no existe. 
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej16()
    COMMENT 'Crear un procedimiento almacenado que me cree una tabla
             denominada usuarios solamente si no existe. '
    BEGIN

      CREATE TABLE IF NOT EXISTS usuarios(
        id int AUTO_INCREMENT,
        nombre varchar(20),
        contrasenia varchar(20),
        nombreUsuario varchar(20),
        PRIMARY KEY(id),
        UNIQUE KEY(nombreUsuario)
        );

    END //
DELIMITER ;

CALL ej16();

/*
  Ej17 - Crear un procedimiento almacenado que me
         inserte datos en la tabla usuarios. 
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej17()
    COMMENT 'Crear un procedimiento almacenado que me
             inserte datos en la tabla usuarios.'
    BEGIN
      INSERT IGNORE INTO  usuarios (nombre, contrasenia, nombreUsuario)
        VALUES ('kevin', 'kevin', 'deonsar'),
               ('pepe', 'pepe', 'usuario1'),
               ('jose', 'jose', 'usuario2'),
               ('fernando','fernando','usuario3');
    END //
DELIMITER ;

CALL ej17();

SELECT * FROM usuarios u;

/*
  Ej18 - Crear un procedimiento almacenado que llame a los procedimientos
         almacenados de los dos ejercicios anteriores.   
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej18()
    BEGIN
      CALL ej16();
      CALL ej17();
    END //
DELIMITER ;

CALL ej18();

/* 
  Ej19 - Crear una función que le pasemos un argumento
         de tipo texto (será el nombre de usuario) y me
         devuelva verdadero en caso de que el usuario exista
         en la tabla usuarios y FALSO en caso de que no.
*/
DELIMITER //
  CREATE OR REPLACE FUNCTION ej19(texto varchar(20))
    RETURNS varchar(20)
  BEGIN
    DECLARE num int DEFAULT 0;

    SELECT COUNT(*) INTO num FROM usuarios u WHERE u.nombreUsuario=texto;

    RETURN IF(num>0,'VERDADERO','FALSO');
  END //
DELIMITER ;

SELECT ej19('deonsar');